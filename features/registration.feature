# @author Abi s205720
Feature: Payment registration feature

  Scenario: Successful payment registration
    Given there is a "MerchantBankAccountRetrieved" event received
    And there is a "CustomerBankAccountRetrieved" event received
    When the "MerchantBankAccountRetrieved" is handled
    And the "CustomerBankAccountRetrieved" is handled
    Then the "TransactionCompleted" event is published

  Scenario: Token Not Validated
    Given there is a "TokenNotValidated" event received
    When the "TokenNotValidated" is handled
    Then the event is removed from the eventMap

  Scenario: Merchant Bank Account Not Found
    Given there is a "MerchantBankAccountNotRetrieved" event received
    When the "MerchantBankAccountNotRetrieved" is handled
    Then the event is removed from the eventMap
