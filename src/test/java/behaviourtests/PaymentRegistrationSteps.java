package behaviourtests;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;

import dtu.ws.fastmoney.*;
import dtuPay.service.PaymentRepository;
import dtuPay.service.model.Client;
import dtuPay.service.model.CorrelationId;
import dtuPay.service.model.Payment;
import dtuPay.service.model.PaymentRequest;
import dtuPay.service.model.PaymentRequestWithAccount;
import dtuPay.service.model.Transaction;
import dtuPay.service.model.ValidatedToken;
import dtuPay.service.model.ValidatedTokenWithAccountId;
import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import dtuPay.service.PaymentService;
import io.cucumber.java.Before;

// @author Cesare s232435
public class PaymentRegistrationSteps {

	private MessageQueue queue = mock(MessageQueue.class);
	private Payment payment = new Payment();
	private PaymentService bankPaymentService = new PaymentService(queue);
	private PaymentRequest paymentRequest = new PaymentRequest();
	private BankService bank = new BankServiceService().getBankServicePort();
	private String customerBankAccountID;
	private String merchantBankAccountID;
	private CorrelationId correlationId = CorrelationId.randomId();

	Client merchant, customer;
	Event customerEvent, merchantEvent, testEvent;
	Event transactionCompletedEvent;
	Transaction transaction;
	PaymentRepository pr;
	ValidatedToken validatedToken;
	String nonexistingPaymentID;
	Exception nonExistingPaymentIdException;

	@Before
	public void setup() throws BankServiceException_Exception {
		String merchantCpr = "12345-321";
		String customerCpr = "12345-123";

		merchant = new Client();
		merchant.setCpr(merchantCpr);
		merchant.setName("joe merchant");
		merchant.generateId();

		customer = new Client();
		customer.setCpr(customerCpr);
		customer.setName("joe customer");
		customer.generateId();

		try {
			bank.retireAccount(bank.getAccountByCprNumber(merchantCpr).getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			bank.retireAccount(bank.getAccountByCprNumber(customerCpr).getId());
		} catch (Exception e) {
			e.printStackTrace();
		}

		User merchantUser = new User();
		merchantUser.setCprNumber(merchantCpr);
		merchantUser.setFirstName("One-M");
		merchantUser.setLastName("Two-M");
		
		User customerUser = new User();
		customerUser.setCprNumber(customerCpr);
		customerUser.setFirstName("One-C");
		customerUser.setLastName("Two-C");

		try {
			merchantBankAccountID = bank.createAccountWithBalance(merchantUser, BigDecimal.valueOf(1000));
			merchant.setAccountId(merchantBankAccountID);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		try {
			customerBankAccountID = bank.createAccountWithBalance(customerUser, BigDecimal.valueOf(1000));
			customer.setAccountId(customerBankAccountID);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Given("there is a {string} event received")
	public void thereIsAEventReceived(String eventName) {

		switch (eventName) {
			case "MerchantBankAccountRetrieved":
				paymentRequest.setMerchantID(merchant.getId());
				paymentRequest.setAmount(100);
				PaymentRequestWithAccount paymentRequestWithAccount = new PaymentRequestWithAccount();
				paymentRequestWithAccount.setPaymentRequest(paymentRequest);
				paymentRequestWithAccount.setMerchantAccountId(merchantBankAccountID);
				merchantEvent = new Event(eventName,
						new Object[] { paymentRequestWithAccount, correlationId });
				break;
			case "CustomerBankAccountRetrieved":
				validatedToken = new ValidatedToken();
				validatedToken.setCustomerID(customer.getId());
				ValidatedTokenWithAccountId validatedTokenWithAccountId = new ValidatedTokenWithAccountId();
				validatedTokenWithAccountId.setCustomerAccountID(customerBankAccountID);
				validatedTokenWithAccountId.setValidatedToken(validatedToken);
				customerEvent = new Event(eventName,
						new Object[] { validatedTokenWithAccountId, correlationId });
				break;
			case "TokenNotValidated":
				customerEvent = new Event(eventName,new Object[]{"",correlationId});
			case "MerchantBankAccountNotRetrieved":
				merchantEvent = new Event(eventName,new Object[]{"",correlationId});
		}
	}

	@When("the {string} is handled")
	public void the_is_handled(String eventName) {
		switch (eventName) {
			case "MerchantBankAccountRetrieved":
				bankPaymentService.handleMerchantAccountRetrieved(merchantEvent);
				break;
			case "CustomerBankAccountRetrieved":
				bankPaymentService.handleCustomerAccountRetrieved(customerEvent);
				break;
			case "TokenNotValidated":
				bankPaymentService.handleTokenNotValidated(customerEvent);
				break;
			case "MerchantBankAccountNotRetrieved":
				bankPaymentService.handleMerchantAccountNotRetrieved(merchantEvent);
				break;
		}
	}

	public void setupSuccessfulTransaction() {
		paymentRequest.setAmount(100);
		paymentRequest.setMerchantID(merchant.getId());
		transaction = new Transaction();
		payment.setCustomerID(customer.getId());
		payment.setPaymentRequest(paymentRequest);
		transaction.setPayment(payment);
		transaction.setMerchantAccountId(merchantBankAccountID);
		transaction.setCustomerAccountId(customerBankAccountID);
	}

	@Then("the {string} event is published")
	public void the_event_is_published(String eventName) {
		 if (eventName.equals("TransactionCompleted")) {
        setupSuccessfulTransaction();
    }

    transactionCompletedEvent = new Event("TransactionCompleted", new Object[] { transaction, correlationId });
    verify(queue).publish(transactionCompletedEvent);
	}

	@After
	public void after() {
		try {
			bank.retireAccount(customerBankAccountID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			bank.retireAccount(merchantBankAccountID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@When("the merchant bank account is retired")
	public void theMerchantBankAccountIsRetired() {
		try {
			bank.retireAccount(merchantBankAccountID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Then("a transaction is initiated and the bank service fails")
	public void aTransactionIsInitiatedAndTheBankServiceFails() throws BankServiceException_Exception {
		try {
			System.out.println("correlation Id " + correlationId);
			bankPaymentService.initiateTransaction(correlationId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Then("a {string} event is thrown")
	public void aEventIsThrown(String event) {
		switch (event) {
			case "MerchantBankAccountNotRetrieved":
				merchantEvent = new Event(event,new Object[]{"",correlationId});
				verify(queue).publish(merchantEvent);
				break;
			case "TokenNotValidated":
				customerEvent = new Event(event,new Object[]{"",correlationId});
				verify(queue).publish(customerEvent);
				break;
			case "PaymentServiceExceptionThrown":
				testEvent = new Event(event,new Object[]{"",correlationId});
				verify(queue).publish(testEvent);
				break;
		}
	}

	@Then("the event is removed from the eventMap")
	public void the_eventMap_does_not_contain_the_correlationId() {
		assertFalse(bankPaymentService.eventMapContains(correlationId));
	}

	@Given("the customer's bank account is retired")
	public void theCustomersBankAccountIsRetired() {
		try{
			bank.retireAccount(this.customerBankAccountID);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@When("a payment with this ID is tried to be retrieved")
	public void aPaymentWithThisIDIsTriedToBeRetrieved() {
		try{
			pr.getPayment(nonexistingPaymentID);
		} catch (Exception e) {
			nonExistingPaymentIdException = e;
			System.out.println(e.getMessage());
		}
	}

	@And("the merchant's bank account is deleted")
	public void theMerchantSBankAccountIsDeleted() {
		try {
			bank.retireAccount(merchantBankAccountID);
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}