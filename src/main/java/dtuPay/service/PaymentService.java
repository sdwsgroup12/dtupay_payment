package dtuPay.service;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import dtuPay.service.model.*;
import messaging.Event;
import messaging.MessageQueue;

import java.math.BigDecimal;
import java.util.concurrent.ConcurrentHashMap;

// @author Alexander s204092
public class PaymentService {

    BankService bank = new BankServiceService().getBankServicePort();

    private ConcurrentHashMap<CorrelationId, InitiateTransactionHandlerClass> eventMap = new ConcurrentHashMap<>();


    private MessageQueue queue;

    public PaymentService(MessageQueue q) {
        queue = q;
        queue.addHandler("MerchantBankAccountRetrieved", this::handleMerchantAccountRetrieved);
        queue.addHandler("CustomerBankAccountRetrieved", this::handleCustomerAccountRetrieved);

        queue.addHandler("TokenNotValidated",this::handleTokenNotValidated);
        queue.addHandler("MerchantBankAccountNotRetrieved",this::handleMerchantAccountNotRetrieved);
    }

    private void addReceivedEventToMap(CorrelationId correlationId, PaymentRequestWithAccount prwa) {
        eventMap.computeIfAbsent(correlationId, k -> new InitiateTransactionHandlerClass())
                .setPaymentRequestWithAccount(prwa);
    }

    private void addReceivedEventToMap(CorrelationId correlationId, ValidatedTokenWithAccountId vtwa) {
        eventMap.computeIfAbsent(correlationId, k -> new InitiateTransactionHandlerClass()).setCustomerIds(vtwa);
    }

    public void handleMerchantAccountRetrieved(Event e){
        CorrelationId correlationId = e.getArgument(1,CorrelationId.class);
        PaymentRequestWithAccount prwa = e.getArgument(0,PaymentRequestWithAccount.class);
        addReceivedEventToMap(correlationId,prwa);
        initiateTransaction(correlationId);
    }

    public void handleCustomerAccountRetrieved(Event e){
        CorrelationId correlationId = e.getArgument(1,CorrelationId.class);
        ValidatedTokenWithAccountId validatedAccountId = e.getArgument(0, ValidatedTokenWithAccountId.class);
        addReceivedEventToMap(correlationId, validatedAccountId);
        initiateTransaction(correlationId);
    }

    public void initiateTransaction(CorrelationId correlationId) {
        InitiateTransactionHandlerClass ith = eventMap.get(correlationId);
        if (!ith.isReady()) {
            return;
        }
        Transaction transaction = ith.createTransaction();
        Payment payment = transaction.getPayment();
        
        try {
            this.bank.transferMoneyFromTo(
                    transaction.getCustomerAccountId(),
                    transaction.getMerchantAccountId(),
                    BigDecimal.valueOf(payment.getPaymentRequest().getAmount()),
                    "Default description");
            PaymentRepository.getInstance().registerPayment(payment);

            Event event = new Event("TransactionCompleted", new Object[]{transaction, correlationId});
            queue.publish(event);
        } catch (Exception exception) {
            exception.printStackTrace();
            queue.publish(
                    new Event("PaymentServiceExceptionThrown", new Object[] { exception.getMessage(), correlationId }));
        }
        eventMap.remove(correlationId);
    }
    public void handleTokenNotValidated(Event e){
        CorrelationId correlationId = e.getArgument(1,CorrelationId.class);
        eventMap.remove(correlationId); // cancel transaction
    }

    public void handleMerchantAccountNotRetrieved(Event e){
        CorrelationId correlationId = e.getArgument(1,CorrelationId.class);
        eventMap.remove(correlationId); // cancel transaction
    }

    public boolean eventMapContains(CorrelationId correlationId){
        return eventMap.contains(correlationId);
    }
}
