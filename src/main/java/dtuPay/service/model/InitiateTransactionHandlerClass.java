package dtuPay.service.model;

import java.io.Serializable;
import java.util.Objects;

public class InitiateTransactionHandlerClass implements Serializable {
    private static final long serialVersionUID = 4311781021432430639L;

    private String merchantAccountId, customerAccountId, customerId;
    private PaymentRequest paymentRequest;

    public void setPaymentRequestWithAccount(PaymentRequestWithAccount prwa){
        this.merchantAccountId = prwa.getMerchantAccountId();
        this.paymentRequest = prwa.getPaymentRequest();
    }
    public String getMerchantAccountId() {
        return merchantAccountId;
    }

    public String getCustomerAccountId() {
        return customerAccountId;
    }
    public void setCustomerIds(ValidatedTokenWithAccountId vtwa) {
        this.customerAccountId = vtwa.getCustomerAccountID();
        this.customerId = vtwa.getValidatedToken().getCustomerID();
    }

    public PaymentRequest getPaymentRequest() {
        return paymentRequest;
    }

    public boolean isReady(){
        return getCustomerAccountId() != null && getMerchantAccountId() != null;
    }

    public Transaction createTransaction() {
        if (!isReady()){
            return null;
        }

        Transaction transaction = new Transaction();
        Payment newPayment = new Payment();
        newPayment.setPaymentRequest(paymentRequest);
        newPayment.setCustomerID(customerId);
        transaction.setPayment(newPayment);
        transaction.setMerchantAccountId(merchantAccountId);
        transaction.setCustomerAccountId(customerAccountId);
        return transaction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InitiateTransactionHandlerClass that = (InitiateTransactionHandlerClass) o;
        return Objects.equals(merchantAccountId, that.merchantAccountId) && Objects.equals(customerAccountId, that.customerAccountId) && Objects.equals(customerId, that.customerId) && Objects.equals(paymentRequest, that.paymentRequest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(merchantAccountId, customerAccountId, customerId, paymentRequest);
    }
}
