package dtuPay.service.model;

import java.io.Serializable;

public class PaymentRequestWithAccount implements Serializable {
    private static final long serialVersionUID = 8133093956290068978L;
    private PaymentRequest paymentRequest;
    private String merchantAccountId;

    public PaymentRequest getPaymentRequest() {
        return paymentRequest;
    }

    public void setPaymentRequest(PaymentRequest paymentRequest) {
        this.paymentRequest = paymentRequest;
    }


    public String getMerchantAccountId() {
        return merchantAccountId;
    }

    public void setMerchantAccountId(String merchantAccountId) {
        this.merchantAccountId = merchantAccountId;
    }

}
