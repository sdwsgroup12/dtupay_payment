package dtuPay.service;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import dtuPay.service.model.Payment;

import java.util.ArrayList;
import java.util.List;

// @author Can s232851
public class PaymentRepository {

    private static PaymentRepository pr = null;
    private List<Payment> payments = new ArrayList<>();
    BankService bank = new BankServiceService().getBankServicePort();

    public PaymentRepository() {
    }
    
    public static PaymentRepository getInstance() {
        if (pr == null) {
            pr = new PaymentRepository();
        }
        return pr;
    }

    public Payment getPayment(String id) throws Exception {
        System.out.println("payments list " + this.payments);
        Payment paymentGot = this.payments.stream().filter(payment -> payment.getPaymentID().equals(id)).findFirst().orElse(null);
        if (paymentGot == null){
            throw new Exception("Payment with id " + id + " not found");
        }
        return paymentGot;
    }

    public Payment registerPayment(Payment payment) {
        System.out.println("payment " + payment);
        this.payments.add(payment);
        return payment;
    }
}
